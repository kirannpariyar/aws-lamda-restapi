package com.kiranpariyar;

import com.amazonaws.serverless.proxy.internal.testutils.MockLambdaContext;
import com.amazonaws.services.lambda.runtime.Context;
import org.hamcrest.core.Is;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ListUserHandlerTest {
    private static Context lambdaContext;

    @Mock
    private UserService userServiceImpl;

    @InjectMocks
    private ListUserHandler handler;

    @BeforeClass
    public static void setUp() {
        lambdaContext = new MockLambdaContext();
    }

    @Test
    public void shouldReturnUserList_handleRequest() {
        List<User> userList = new ArrayList<User>(){{
            add(new User(1L,"chrish","evans"));
            add(new User(2L,"scarlet","johnson"));
        }};

        Mockito.when(userServiceImpl.findAll()).thenReturn(userList);
        ApiGatewayResponse apiGatewayResponse = handler.handleRequest(new HashMap<String, Object>(), lambdaContext);
        assertEquals(200, apiGatewayResponse.getStatusCode());
        assertThat(apiGatewayResponse.getBody().isEmpty(), Is.is(false));
    }
}
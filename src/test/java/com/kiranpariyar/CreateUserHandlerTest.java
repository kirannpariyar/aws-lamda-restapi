package com.kiranpariyar;

import com.amazonaws.serverless.proxy.internal.testutils.MockLambdaContext;
import com.amazonaws.services.lambda.runtime.Context;
import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CreateUserHandlerTest {

    private static Context lambdaContext;

    @Mock
    private UserService userServiceImpl;

    @InjectMocks
    private CreateUserHandler handler;

    @BeforeClass
    public static void setUp() {
        lambdaContext = new MockLambdaContext();
    }

    @Test
    public void shouldReturnSuccessStatus_handleRequest() {
        Map<String, Object> input = new HashMap<String, Object>(){{put(
                "body", "{\"email\": \"Hello\", \"fullname\": \"World\"}");}};
        Mockito.when(userServiceImpl.save(Matchers.any(User.class))).thenReturn(Matchers.any(User.class));
        ApiGatewayResponse apiGatewayResponse = handler.handleRequest(input, lambdaContext);
        assertEquals(200, apiGatewayResponse.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestStatus_handleRequest() {
        Map<String, Object> input = new HashMap<String, Object>(){{put(
                "body","hello");}};
        ApiGatewayResponse apiGatewayResponse = handler.handleRequest(input, lambdaContext);
        assertEquals(400, apiGatewayResponse.getStatusCode());
    }

}
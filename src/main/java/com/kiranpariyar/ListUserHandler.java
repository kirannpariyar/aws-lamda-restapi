package com.kiranpariyar;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public class ListUserHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private final Logger logger = Logger.getLogger(this.getClass());

    private UserService userService = new UserServiceImpl();

	public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
    try {
        List<User> users = userService.findAll();
        return ApiGatewayResponse.builder()
    				.setStatusCode(200)
    				.setObjectBody(users)
    				.build();
    } catch (Exception ex) {
        logger.error("Error in listing users: " + ex);
		Response responseBody = new Response("Error in listing users: ", input);
		return ApiGatewayResponse.builder()
				.setStatusCode(500)
				.setObjectBody(responseBody)
				.build();
    }
	}
}

package com.kiranpariyar;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Map;

public class CreateUserHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private final Logger logger = Logger.getLogger(this.getClass());

    private UserService userService = new UserServiceImpl();

    private User mapInputToUser(Map<String, Object> input) throws IOException{
        JsonNode body = new ObjectMapper().readTree((String) input.get("body"));
        User user = new User();
        user.setEmail(body.get("email").asText());
        user.setFullName(body.get("fullname").asText());
        return user;
    }

    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
        try {
            User user = mapInputToUser(input);
            user = userService.save(user);
            return ApiGatewayResponse.builder()
                    .setStatusCode(200)
                    .setObjectBody(user)
                    .build();
        } catch (IOException ex) {
            logger.error("Error in saving user");
            Response responseBody = new Response("Error while saving user: ", input);
            return ApiGatewayResponse.builder()
                    .setStatusCode(400)
                    .setObjectBody(responseBody)
                    .build();
        }
    }
}

package com.kiranpariyar;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private final Logger logger = Logger.getLogger(this.getClass());

    public User save(User user) throws InternalServerError{
        try {
            Connection connection = DatabaseConnector.getConnection();
            String query = "insert into users (email, fullname)" + " values (?, ?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString (1, user.getEmail());
            preparedStmt.setString (2, user.getFullName());
            preparedStmt.execute();
            ResultSet rs=preparedStmt.getGeneratedKeys();
            Long userId = null;
            if(rs.next()){
                userId = rs.getLong(1);
            }
            user.setId(userId);
            connection.close();
            return user;
        } catch (SQLException e1) {
            logger.error("Error While saving user");
            throw new InternalServerError();
        }
    }

    public List<User> findAll() throws InternalServerError{
        try {
            Connection connection = DatabaseConnector.getConnection();
            List<User> users = new ArrayList<>();
            Statement stmt = connection.createStatement();
            ResultSet rs=stmt.executeQuery("select * from users");
            while(rs.next()){
                User user = new User(rs.getString(2), rs.getString(3));
                users.add(user);
            }
            return users;
        } catch (SQLException e1) {
            logger.error("Error While fetching users");
            throw new InternalServerError();
        }
    }
}

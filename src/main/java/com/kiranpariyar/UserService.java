package com.kiranpariyar;

import java.util.List;

public interface UserService {

    User save(User user);

    List<User> findAll();
}

package com.kiranpariyar;

public class InternalServerError extends RuntimeException{

    public InternalServerError(){
        super("Something went wrong in the system.");
    }

    public InternalServerError(String message){
        super(message);
    }

    public InternalServerError(String message, Throwable throwable){
        super(message, throwable);
    }
}

package com.kiranpariyar;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String HOST = System.getenv("dbHost");
    private static final String PORT = System.getenv("dbPort");
    private static final String DB_NAME = System.getenv("dbName");
    private static final String USERNAME = System.getenv("dbUsername");
    private static final String PASSWORD = System.getenv("dbPassword");

    private static final Logger logger = Logger.getLogger(DatabaseConnector.class);

    public static Connection getConnection() throws InternalServerError{
        try {
            Class.forName(JDBC_DRIVER);
            String DB_URL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DB_NAME;
            return DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
        } catch (SQLException | ClassNotFoundException e1) {
            logger.error("Error while connecting to database");
            throw new InternalServerError();
        }
    }
}
